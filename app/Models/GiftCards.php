<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Datatables;
use Carbon\Carbon;

class GiftCards extends Model {

    protected $fillable = [
        'title',
        'description',
        'quantity',
        'price',
        'status',        
        'code'
    ];
    
    public function Files()
    {
      return $this->morphMany('App\Models\Files', 'imageable');
    }
    public function saveCard($data, $id = NULL) {   
        
        if (isset($id) && !empty($id)) {            
            unset($data['_token']);
            unset($data['_method']);
            unset($data['code']);
            //if (isset($data['image']) && empty($data['image'])) {
                $image=$data['image'];
                unset($data['image']);
            //}                        
            $giftcard=$this->where('id', $id)->first();
            $giftcard->update($data);              
            if(isset($image) && !empty($image) && $image[0]!=''){
                $giftcard->Files()->update($image); 
            }            
            return true;
        }
        $data['code']=  encrypt($data['code']);                    
        
        $giftCard=$this->create($data);
        $giftCard->Files()->create($data['image']);
        return $giftCard;
    }

    public function getGiftCards($datatable = false, $id = NULL) {
        
        if ($datatable == true) {
            $data=$this->with('Files')->get();                                    
            foreach ($data as $key => $val) {                              
                if(isset($data[$key]->image) && !empty($data[$key]->image)){
                    $data[$key]->image=getImageByPath($val->Files->first()->path,'thumbnail');
                }
                else{
                    $data[$key]->image='';
                }
            }    
            return getAuthorizedButton($data)->toJson();            
        } else if (isset($id) && !empty($id)) {           
            return $this->where('id', $id)->first();
        } else {
            return $this->get();
        }
    }

    public function deleteGiftcard($id) {
        if (isset($id) && !empty($id)) {
            return $this->where('id', $id)->delete();
        }
        return trans('message.failure');
    }

}
