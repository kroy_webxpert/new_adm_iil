<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\forumsRequest;
use App\Http\Controllers\Controller;
use App\Models\Forums;
use App\Models\EmployeeDepartments;
use App\Models\AdminUser;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Datatables;
use DB;

class ForumsController extends Controller {
    
    public $Forums;
    public $EmployeeDepartments;

    public function __construct() {
        $this->Forums = new Forums();
        $this->EmployeeDepartments = new EmployeeDepartments();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $page_title = "{trans('form.forums.forum_title')}";
        $page_description = 'Listing of all topics';
        return view('admin.forums.index', compact('page_title', 'page_description'));
    }

    public function datatableList(Request $request) {
        
        $forums=$this->Forums->getForums();
//        echo "<PRE>";
//        print_r($forums);die;
        return Datatables::of($forums)
                        ->addColumn('action', function ($forum) {
                            return '<a href="' . route(config('project.admin_route').'forums.edit', encrypt($forum->id)) . '" class="" data-toggle="tooltip" data-placement="top" title="Edit">View</i></a>' .
                                    '&nbsp;&nbsp;<a href="javascript:void(0)" class="deleteCategory" data-toggle="modal" data-placement="top" title="Delete" data-category_delete_remote="' . route(config('project.admin_route').'forums.destroy', encrypt($forum->id)) . '">Unpublished</a>';
                        })
                        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $department['all_departmentsnames']=$this->EmployeeDepartments->getEmployeeDepartmentsnames();
        return view('admin.forums.create', compact('department'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(forumsRequest $request) {
        $data = $request->only('topic_name', 'topic_department_id');   
        $data['admin_users_id']= \Auth::guard('admin')->user()->id;
        $incrementdepartment=$this->EmployeeDepartments->incrementDepartmentTopic($data['topic_department_id']);
        $forums=$this->Forums->saveForum($data);        
        \Flash::success(trans('message.forums.add_success'));
        if ($request->ajax()) {
            return response()->json([
                        'status' => 'success',
                        'redirectUrl' => route(config('project.admin_route').'forums.index'),
            ]);
        } else {
            return redirect()->route(config('project.admin_route').'forums.index');
        }
    }
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $id = decrypt($id);
        $department['all_departmentsnames']=$this->EmployeeDepartments->getEmployeeDepartmentsnames();
        $department['department_name']=$this->EmployeeDepartments->getDepartmentwithtopic($id);
        $all_forums=$this->Forums->getForums();
        $forums=$this->Forums->getForums($id);        
        return view('admin.forums.edit', compact('forums', 'all_forums','department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(forumsRequest $request, $id) {        
        $id = decrypt($id);
        $forums=$this->Forums->getForums($id);        
        
        $data = $request->only('topic_name', 'topic_department_id');
        $forums=$this->Forums->saveForum($data,$id); 

        \Flash::success(trans('message.forums.update_success'));

        if ($request->ajax()) {
            return response()->json([
                        'status' => 'success',
                        'redirectUrl' => route(config('project.admin_route').'forums.index'),
            ]);
        } else {
            return redirect()->route(config('project.admin_route').'forums.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id, Request $request) {
        $id = decrypt($id);
        try {
            $forums=$this->Forums->getForums($id);
            $decrementdepartment=$this->EmployeeDepartments->decrementDepartmentTopic($forums['topic_department_id']);
            $data['status'] = 'Inactive';
            $forums=$this->Forums->saveForum($data,$id); 
            //$this->Forums->deleteForums($id);
            if ($request->ajax()) {
                return response(['msg' => trans('message.forums.delete_success'), 'success' => 1]);
            }
        } catch (\Illuminate\Database\QueryException $ex) {
            return response(['msg' => $ex->errorInfo, 'success' => 0]);//$ex->getMessage()
        }
        
    }

}
