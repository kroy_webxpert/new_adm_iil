<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PromotionsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {        
        if($id=$this->route('product_conditions', 'id')){            
            $id = decrypt($id);
        }
        return [                            
            //'discount_type' => 'unique:promotions,name,'. @$id ,
            "discount_type" => "required",
            "discount" => "required|numeric",
            "start_date" => "required",
            "end_date" => "required",
            //"promo_code" => "unique:promotions,promo_code,". @$id ,
            'users' => 'required',            
            'selected_users' => 'required_if:users,select',            
            "promo_code" => "required",            
            
        ];    
    }
}
