<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EmployeeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($id = $this->route('employee', 'id'))
        {
            $id = decrypt($id);
            return [
                'employee_code' => 'required|unique:admin_users,employee_code,'.@$id.',id,deleted_at,NULL',
                'professional_email' => 'required|string|email|unique:admin_users,professional_email,'.@$id.',id,deleted_at,NULL',
                'personal_email' => 'required|string|email|unique:admin_users,personal_email,'.@$id.',id,deleted_at,NULL',
                'role_id' => 'required|not_in:0',
                'contact_number' => 'numeric',
                'secret_question_id' => 'required|not_in:0',
                'secret_answer' => 'required',
                'dob'=>'required|date',
                'photo'=> 'mimes:jpg,png,jpeg,gif|min:5'
            ];
        }
        else
        {
            return [
                'employee_code' => 'required|unique:admin_users,employee_code,NULL,employee_code,deleted_at,NULL',
                'professional_email' => 'required|string|email|unique:admin_users,professional_email,NULL,professional_email,deleted_at,NULL',
                'personal_email' => 'required|string|email|unique:admin_users,personal_email,NULL,personal_email,deleted_at,NULL',
                'password'         => 'required|min:7|max:14',
                'confirm_password' => 'required|same:password|min:7|max:14',
                'role_id' => 'required|not_in:0',
                'contact_number' => 'numeric',
                'secret_question_id' => 'required|not_in:0',
                'secret_answer' => 'required',
                'dob'=> 'required|date',    
                'photo'=> 'mimes:jpg,png,jpeg,gif|min:5'
            ];
        }        
    }
}
