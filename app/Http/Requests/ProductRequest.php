<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProductRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:demoproducts',
            'manufacturer' => 'required',
            'description' => 'required',
            'price' => 'required|numeric',
            'is_return_applicable' => 'required',
            'is_warranty_applicable' => 'required',
            'status' => 'required',
            'photo.*' => 'mimes:jpeg,jpg,png|max:1024',
            'category_id' => 'required',            
        ];
    }
}
