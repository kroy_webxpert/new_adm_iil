<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProductConditionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
        if($id=$this->route('product_conditions', 'id')){            
            $id = decrypt($id);
        }
        return [                            
            'name' => 'unique:product_conditions,name,'. @$id ,
            "description" => "required",
        ];        
    }
}
