{{--@if (count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
@endforeach
</ul>
</div>
@endif--}}

<!-- BEGIN FORM-->
<div  class="form-body">
    <div class="form-group {!! $errors->has('attribute_set_id') ? 'has-error' : '' !!}">
        {!! Form::label('attribute_set_id', trans("attribute.attribute_set_id"), ['class' => 'col-md-3 control-label']) !!}    
        <div class="col-md-4">
            {!! Form::select('attribute_set_id', (['0' => 'Select a AttributeSet']+$input['all_Attributeset']),null,['class' => 'form-control']) !!}
            {!! $errors->first('attribute_set_id', '<span class="help-block">:message</span>') !!}
        </div>
    </div>
    <div class="form-group required {!! $errors->has('attribute_name') ? 'has-error' : '' !!}">
        {!! Form::label('attribute_name', trans("attribute.attribute_name"), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-4">
            {!! Form::text('attribute_name', null, ['class'=>'form-control', 'placeholder'=>"Enter Attribute Name"]) !!}
            {!! $errors->first('attribute_name', '<span class="help-block">:message</span>') !!}
        </div>
    </div>
    <div class="form-group required {!! $errors->has('attribute_set_description') ? 'has-error' : '' !!}">
        {!! Form::label('attribute_description', trans("attribute.attribute_description"), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-4">
            {!! Form::textarea('attribute_description', null, ['class'=>'form-control']) !!}
            {!! $errors->first('attribute_description', '<span class="help-block">:message</span>') !!}
        </div>
    </div> 
    <div class="form-group {!! $errors->has('catelog_input_type') ? 'has-error' : '' !!}">
        {!! Form::label('catelog_input_type', trans("attribute.catelog_input_type"), ['class' => 'col-md-3 control-label']) !!}    
        <div class="col-md-4">
            {!! Form::select('catelog_input_type', (['dropdown' => 'Drop Down','radio' => 'Radio Button']),null,['class' => 'form-control']) !!}
            {!! $errors->first('catelog_input_type', '<span class="help-block">:message</span>') !!}
        </div>
    </div>
    <div class="form-group {!! $errors->has('view_in_filter') ? 'has-error' : '' !!}">
        {!! Form::label('view_in_filter', trans("attribute.view_in_filter"), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-4">            
            <label>
                {!! Form::radio('view_in_filter', 1,true, ['class'=>'form-control', 'style' => ' width : 18% !important; float:left; ' ]) !!}
                <div style="float: left;margin-top: 20%;margin-left: 5px;">&nbsp;Yes</div>
            </label>
            <label>
                {!! Form::radio('view_in_filter', 0,false, ['class'=>'form-control','style' => ' width : 18% !important; float:left; ']) !!}
                <div style="float: left;margin-top: 20%;margin-left: 5px;">&nbsp;No</div>
            </label>
            {!! $errors->first('view_in_filter', '<span class="help-block">:message</span>') !!}
        </div>
    </div>
    <div class="form-group {!! $errors->has('comparable') ? 'has-error' : '' !!}">
        {!! Form::label('comparable', trans("attribute.comparable"), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-4">
            <label>
                {!! Form::radio('comparable', 1, true ,['class'=>'form-control', 'style' => ' width : 18% !important; float:left; ']) !!}
                <div style="float: left;margin-top: 20%;margin-left: 5px;">&nbsp;Yes</div>
            </label>
            <label>
                {!! Form::radio('comparable', 0, false,['class'=>'form-control', 'style' => ' width : 18% !important; float:left; ']) !!}
                <div style="float: left;margin-top: 20%;margin-left: 5px;">&nbsp;No</div>
            </label>
            {!! $errors->first('comparable', '<span class="help-block">:message</span>') !!}
        </div>
    </div>
    <div class="form-group {!! $errors->has('mytext.*') ? 'has-error' : '' !!} required">
        {!! Form::label('attribute_values', trans("attribute.attribute_values"), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-4">
            <button class="add_field_button form-control" style="float:right; width:35%;">Add More</button>            
            <div class="input_fields_wrap">
            <?php 
            if(!empty($input['attribute_values']))
            {
                $flag = 0;
                foreach( $input['attribute_values'] as $values )
                {
                    if($flag == 0)
                    {?>
                     <div>
                         {!! Form::text('mytext['.$flag.']', $values->attribute_values, ['class'=>'form-control mytext.'.$flag ,'style'=> 'float: left;width:60%;']) !!}
                     </div>
                    <?php }else{?>
                    <div style="float: left;width : 100%; ">
                         <a href="#" class="remove_field form-control btn btn-danger btn-xs fa fa-trash-o deleteAttribute" title="Delete" data-toggle="modal" data-placement="top" style="float:right; width:20%;margin-top: 5px;font-size: 18px;"></a>
                         {!! Form::text('mytext['.$flag.']', $values->attribute_values, ['class'=>'form-control mytext.'.$flag,'style'=> 'float: left;width:60% !important;margin-top: 5px;']) !!}                         
                     </div>   
                    <?php } ?>
                <?php 
                    $flag++;
                } 
            }
            else{ ?>
                <div>
                  {!! Form::text('mytext[0]', null, ['class'=>'form-control mytext.0','style'=> 'float: left;width:60%;']) !!}
                </div>
            <?php } ?>
            </div>
            
        </div>  
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit(isset($model) ? trans("attribute.update") : trans("attribute.save"), ['class'=>'btn btn-primary']) !!}
            <a class="btn default" href="{{route(config('project.admin_route').'attribute.index')}}">Cancel</a>
        </div>
    </div>
</div>
<style>
    .arraycls{ float: left !important; }
</style>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script>
    //var max_fields      = 30; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    
    var counter = 2;
    
    $(add_button).click(function(e){ //on add input button click
        counter++;
        e.preventDefault();                        
        $(wrapper).append('<div style="float: left;width : 100%; " ><a href="#" class="remove_field form-control btn btn-danger btn-xs fa fa-trash-o deleteAttribute" title="Delete" data-toggle="modal" data-placement="top" style="float:right; width:20%;margin-top: 5px;font-size: 18px;"></a><input type="text" name="mytext[a'+counter+']" class="form-control mytext.a'+counter+'" style="float: left;width:60%;margin-top: 5px;"></div>'); //add input box        
    });
   
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault();         
        $(this).parent('div').remove();
    })   
</script>
<!-- END FORM-->