<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Model::unguard();

        //$this->call('DemoCategoriesTableSeeder');
        //$this->call('DemoProductsTableSeeder');

        // $this->call(UsersTableSeeder::class);
        
        DB::table('admin_users')->insert([
            'first_name' => 'Super',
            'last_name' => 'Admin',
            'professional_email' => 'sa@devanche.com',
            'personal_email' => 'sa@gmail.com',
            'password' => Hash::make('admin@123') ,
            'gender' => 'Male',
            'dob' => '1985-01-01',
            'role_id' => 1,
            'department_id' => 1,
            'status' => 'Active',
            'confirmed' => 1,
            'created_at' => '2016-05-24 15:40:38'
        ]);
        
        
        
        /*DB::table('admins')->insert([
            'name' => 'SuperAdmin',
            'email' => 'sa@gmail.com',
            'password' => bcrypt('admin123') ,
        ]);*/
    }
    

}

class DemoCategoriesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('democategories')->delete();

        $faker = Faker\Factory::create();
        for ($i = 0; $i < 200; ++$i) {
            $demoCategory = new \App\Models\Democategory();
            $demoCategory->title = $faker->title;
            $demoCategory->parent_id = rand(1, 1000);
            $demoCategory->description = str_random(50);
            $demoCategory->status = 'Active';
            $demoCategory->save();
        }
    }
}
class DemoProductsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('demoproducts')->delete();

        $faker = Faker\Factory::create();
        for ($i = 0; $i < 200; ++$i) {
            $demoCategory = new \App\Models\Demoproduct();
            $demoCategory->name = $faker->name;
            $demoCategory->manufacturer = str_random(10);
            $demoCategory->description = str_random(50);
            $demoCategory->status = 'Active';
            $demoCategory->price = rand(50,2500);
            $demoCategory->save();
        }
    }
}
